require_relative 'tangento_api'
invoice = TangentoAPI.new().call('order_invoice.info', ARGV[0])
field_set = TangentoAPI.getConfig('invoice.yaml')

puts "=========Header========="
field_set['header_fields'].each do |l|
  TangentoAPI.puts_kvalue(invoice,l)
end

puts "\n=========Items========="
invoice['items'].each do |li|
  field_set['item_fields'].each do |q|
      TangentoAPI.puts_kvalue(li,q)
  end
  puts "----------------"
end
require 'nokogiri'
require_relative 'tangento_api'
magento = TangentoAPI.new()
xmlBuilder = Nokogiri::XML::Builder.new do |xml|
  xml.OrderTrackings('xmlns'=>'http://mage.tandev.net/fakeyNameSpace') {
    magento.call('sales_order.list', {:status=>'pending'}).each do |order_header|
      order = magento.call('sales_order.info', order_header['increment_id'])
      xml.OrderTracking {
        xml.WarehouseId '235'
        xml.ClientId '10040'
        xml.ClientOrderNum order['increment_id']
        xml.DCOrderNum 'F101724735001'
        
        xml.Package {
          xml.ShippingCarrier 'ups'
          xml.ShippingServiceLevel 'GND51'
          xml.TrackingNum '1Z012345678901%04d' % [ rand(9999) ]
          xml.DCCartonId '%07dPRF' % [ rand(9999) ]
          xml.ShippedDateTime Time.now.strftime('%Y%m%d')
      }
    }
    end
  }
end
puts xmlBuilder.to_xml
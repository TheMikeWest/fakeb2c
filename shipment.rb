require_relative 'tangento_api'
require 'php_serialize'
shipment = TangentoAPI.new().call('sales_order_shipment.info', ARGV[0])
field_set = TangentoAPI.getConfig('shipment.yaml')

puts "=========Header========="
field_set['header_fields'].each do |l|
  TangentoAPI.puts_kvalue(shipment,l)
end
# packages, stored in the "header", so to speak, are specially
# handled here, as they are specially serialized way down in the
# undocumented bowels of Magento:
puts "=========Header=>Packages========="
PHP.unserialize(shipment['packages']).each do |package|
  package.each do |key, value|
    TangentoAPI.puts_kvalue(package,key)
  end
end

puts "\n=========Items========="
shipment['items'].each do |li|
  field_set['item_fields'].each do |field|
    TangentoAPI.puts_kvalue(li,field)
  end
  puts
end
puts "\n=========Tracks========="
shipment['tracks'].each do |track|
  field_set['track_fields'].each do |field|
    TangentoAPI.puts_kvalue(track,field)
  end
end

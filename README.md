# fakeb2c 
## A misnamed suite of Magento Order/ Invoice/ Shipment Exercise Tools

To make any of this do anything interesting, first edit your tangento_api.yaml file to point at your Magento Installation with the appropriate credentials. You can also modify the programs themselves to pass in a yaml configuration file of your choice.

## tangento_api.rb
Magento XMLRPC Wrapper

### Parameters
Are taken from the tangento_api.yaml file

## order.rb
Dumps a specific order

### Parameters

#### ARGV[0]
Pass in the order number

#### order.yaml
Contains two yaml collections, 1 called "header_fields," and another called "item_fields". These specify exactly which fields order.rb will dump out.

## invoice.rb
Dumps the specified invoice

### Parameters

#### ARGV[0]
Pass in the invoice number

#### invoice.yaml
Contains two yaml collections, 1 called "header_fields," and another called "item_fields". These specify exactly which fields invoice.rb will dump out.

## shipment.rb
Dumps the specified shipment

### Parameters

#### ARGV[0]
Pass in the shipment number

#### shipment.yaml
Contains three (3) yaml collections
* **header_fields** fields from the basic shipment itself
* **item_fields** fields from the collection of items for this shipment
* **track_fields** fields from the collection of tracking details for this shipment

## fakeOrderCancel.rb
Dummies up an XML file that looks like a cancelation file. This pulls in all orders from the targeted Magento site that are in a 'pending' status.

## fakeOrderShipment.rb
Dummies up an XML file that looks like a cancelation file. This pulls in all orders from the targeted Magento site that are in a 'pending' status.

## fakeOrderTracking.rb
An add-on to Shipment feeds. Due to a limitation
in the source feed, multiple tracking numbers must be handled in a separate feed file containing only
tracking numbers. This program simulates that feed.
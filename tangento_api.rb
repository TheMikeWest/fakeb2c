# Copyright Camptocamp SA 2012
# License: AGPL (GNU Affero General Public License)[http://www.gnu.org/licenses/agpl-3.0.txt]
# Author Guewen Baconnier

require 'xmlrpc/client'
require 'yaml'
require 'pp'

v, $VERBOSE = $VERBOSE, nil
XMLRPC::Config::ENABLE_NIL_PARSER = true
$VERBOSE=v
class TangentoAPI
  TANGENTO_CONFIG_FILE = 'tangento_api.yaml'
  attr_accessor :url, :api_user, :api_key, :session_id
  
  def initialize(options={})
    config = YAML.load_file(options[:config_file] || TANGENTO_CONFIG_FILE)
    @url = config['base_url'] << '/api/xmlrpc/'
    @api_user = config['api_user']
    @api_key = config['api_key']
    @debug = options[:debug] || false
    if @debug == true
      puts 'Connecting to: ' << @url
      puts 'as ' << @api_user
    end
    @client = init_client
    @session_id = get_session_id
  end
  
  def call(method, *arguments)
    @client.call('call', @session_id, method, arguments)
  end
  
  class << self
    def puts_kvalue(a,l)
      if !a[l].nil?
        puts "%-20.20s => %25.25s" % [l, a[l]]
      end
    end
    
    def getConfig(config_file)
      YAML.load_file(config_file)
    end
  end
  
  private
  
  def init_client
    client = XMLRPC::Client.new2(@url)
    http_debug(@debug)
    client.set_debug
    client
  end
  
  def http_debug(active)
    output = active ? $stderr : false
      
    XMLRPC::Client.class_eval do
      define_method :set_debug do
        @http.set_debug_output(output)
      end
    end
  end
  
  def get_session_id
    @client.call('login', @api_user, @api_key)
  end
end
require_relative 'tangento_api'

order = TangentoAPI.new().call('sales_order.info', ARGV[0])
field_set = TangentoAPI.getConfig('order.yaml')

puts "=========Header========="
field_set['header_fields'].each do |l|
  TangentoAPI.puts_kvalue(order,l)
end

puts "\n=========Items========="
order['items'].each do |li|
  if li['product_type'].eql? 'simple'
    puts "\n**Child!\n"
  end
  field_set['item_fields'].each do |q|
      TangentoAPI.puts_kvalue(li,q)
  end
end
require 'nokogiri'
require_relative 'tangento_api'
magento = TangentoAPI.new()
xmlBuilder = Nokogiri::XML::Builder.new do |xml|
  xml.OrderShipments('xmlns'=>'http://mage.tandev.net/fakeyNameSpace') {
    magento.call('sales_order.list', {:increment_id=>'100000739'}).each do |order_header|
      order = magento.call('sales_order.info', order_header['increment_id'])
      xml.OrderShipment {
        xml.WarehouseId '235'
        xml.ClientId '10040'
        xml.ClientOrderNum order['increment_id']
        xml.DCOrderNum 'F'+order['increment_id']

        order['items'].each do |i|
          xml.Detail {
            xml.RecordType 'S'
            xml.TransactionDateTime Time.now.strftime('%Y%m%d%H%M%S')
            xml.ClientOrderLine i['item_id']
            xml.ClientOrderPartNum i['sku']
            xml.QtyShipped i['qty_ordered'].to_i
            xml.QtyCancelled i['qty_canceled'].to_i
            xml.CancelReasonCode
          }
        end

        xml.Package {
          xml.ShippingCarrier 'ups'
          xml.ShippingServiceLevel 'GND51'
          xml.TrackingNum '1Z%016d' % [rand(99999)]
          xml.DCCartonId '%07dPRF' % [rand(99999)]
          xml.ShippedDateTime Time.now.strftime('%Y%m%d')
          order['items'].each do |i|
              xml.PackageDetail {
              xml.ClientOrderLine i['item_id']
              xml.ClientOrderPartNum i['sku']
              xml.QtyShipped i['qty_ordered'].to_i
          }
        end
      }
    }
    end
  }
end
puts xmlBuilder.to_xml
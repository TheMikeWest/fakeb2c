require 'nokogiri'
require_relative 'tangento_api'
order = TangentoAPI.new().call('sales_order.info', ARGV[0])
xmlBuilder = Nokogiri::XML::Builder.new do |xml|
  xml.OrderShipments('xmlns'=>'http://mage.tandev.net/fakeyNameSpace') {
    xml.OrderShipment {
      xml.WarehouseId '235'
      xml.ClientId '10040'
      xml.ClientOrderNum order['increment_id']
      xml.DCOrderNum 'F101724735001'

      order['items'].each do |i|
        xml.Detail {
          xml.RecordType 'C'
          xml.TransactionDateTime Time.now.strftime('%Y%m%d%H%M%S')
          xml.ClientOrderLine i['item_id']
          xml.ClientOrderPartNum i['sku']
          xml.QtyShipped 0
          xml.QtyCancelled i['qty_ordered'].to_i / 3
          xml.CancelReasonCode 'X'
        }
      end
  }
}
end
puts xmlBuilder.to_xml

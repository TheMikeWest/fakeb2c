require_relative 'tangento_api'
require 'php_serialize'

magento = TangentoAPI.new()
parms = {:status=>'pending' }
order_list = magento.call('sales_order.list', parms)

order_list.each do |one_order|
  order = magento.call('sales_order.info', one_order['increment_id'])
  
  order.each do |magefieldset,value|
    if !value.nil?
      if value.respond_to?(:each)
        puts "--#{magefieldset}--\n"
        value.each do |innerfield, innervalue|
          if !innervalue.nil?
            puts "\t#{innerfield}=#{innervalue}\n"
          end
        end
      else
        puts "#{magefieldset}=#{value}\n"
      end
    end
  end
  
  puts "-- let's play with items --\n"
  items = order['items'];
  items.each do |lineitem|
    if !lineitem.nil?
      lineitem.each do |fieldname,value|
      begin
        numeric_value = Integer(value)
        its_a_string = false
      rescue Exception => e 
        its_a_string = true
      end
      if (!value.nil? && its_a_string) || (!its_a_string && numeric_value != 0)
        puts "#{fieldname}\t#{value}\n"
        if fieldname.eql? 'product_options'
          puts '--product_options--'
          pp PHP.unserialize(value)
        end
        end
      end
    end
  end
  exit
end
